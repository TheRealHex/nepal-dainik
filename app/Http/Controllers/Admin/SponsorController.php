<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Sponsor;
use Illuminate\Http\Request;

class SponsorController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
          'title' => 'required',
          'tags' => 'required',
          'image.*' => 'image|mimes:jpeg, png, jpg,gif, svg|max:2048*',
          'content' => 'required'
      ]);
        if ($files = $request->file('image')) {
       // Define upload path
           $destinationPath = public_path('/image/'); // upload path
 // Upload Orginal Image           
           $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $profileImage);
           $fileNameToStore = $profileImage;
       }else{
          $fileNameToStore='';
      }
      $post = new Post();
      $post->title = $request->input('title');
      $post->image = $fileNameToStore;
      $post->content = $request->input('content');
      $post->cat_id = $request->input('cat_id');
      $post->user_id = Auth()->id();
      $post->tag = $request->input('tags');
      $post->breaking = $request->input('breaking');
      $post->save();
      return redirect()->route('post.index')->with('status','Post added on pending.');
  }

  public function index()
  {
    $sponsor=Sponsor::where('status', '=', 'pending')->get();
    return view('admin.sponsor-mgmt',compact('sponsor'));
}
public function approved()
{
    $sponsor=Sponsor::where('status', '=', 'approved')->get();
    return view('admin.sponsor-mgmt',compact('sponsor'));
}
public function declined()
{
    $sponsor=Sponsor::where('status', '=', 'declined')->get();
    return view('admin.sponsor-mgmt',compact('sponsor'));
}
public function status(Request $request, $id)
{
    $sponsor = Sponsor::find($id);
    $sponsor->status = $request->input('status');
    $sponsor->update();
    return redirect()->back()->with('status','Updated successfully.');
}
}
